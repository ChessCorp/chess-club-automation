#!/bin/bash

set -e -x

export PATH=/usr/local/bin:$PATH

BASE=$(readlink -f $(dirname $0)/../..)
SCRIPT_BASE=$(readlink -f $(dirname $0))
CLUB_UI_BASE=$BASE/chess-club-ui
CLUB_BASE=$BASE/chess-club
RUNTIME=$BASE/runtime

### Initial folders setup

if test ! -d ${RUNTIME}
then
    mkdir ${RUNTIME}
fi

if test ! -d ${CLUB_BASE}
then
    echo "Could not find club at ${CLUB_BASE}, downloading it"
    git clone https://github.com/ChessCorp/chess-club.git ${CLUB_BASE}
fi

if test ! -d ${CLUB_UI_BASE}
then
    echo "Could not find UI at ${CLUB_UI_BASE}"
    git clone https://github.com/ChessCorp/chess-club-ui.git ${CLUB_UI_BASE}
fi


echo "Shutting down club"
${SCRIPT_BASE}/shutdown.sh

###
### Build UI
###

cd ${CLUB_UI_BASE}
git pull --rebase

if test -d node_modules
then
    echo "[Warn] not updating node modules to preserve build time"
#	npm update
else
	npm install
fi

if test -d bower_components
then
    echo "[Warn] not updating bower components to preserve build time"
else
    bower install
fi

gulp clean default
rsync -ad dist/ ${CLUB_BASE}/src/main/webapp

###
### Build club
###

RELEASE=$(date '+%Y-%m-%d')

cd ${CLUB_BASE}
git reset --hard
git pull --rebase
sed -i -e "s/SNAPSHOT/${RELEASE}/" pom.xml
mvn clean package -DskipTests=true -DskipChessUIDownload=true -Ppostgresql

###
### Run club server
###

cd ${RUNTIME}
${SCRIPT_BASE}/startup.sh
