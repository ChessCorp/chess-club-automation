#!/bin/bash

set -e -x

export PATH=/usr/local/bin:$PATH

BASE=$(readlink -f $(dirname $0)/../..)
SCRIPT_BASE=$(readlink -f $(dirname $0))
CLUB_BASE=$BASE/chess-club
RUNTIME=$BASE/runtime


cd ${RUNTIME}
nohup sudo java -jar ${CLUB_BASE}/target/chess-club*.jar >/dev/null 2>&1 &
