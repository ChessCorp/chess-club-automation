#!/bin/bash

MAIL_USER=$1
MAIL_PASS=$2

if test "$1" == "" -o "$2" == ""
then
	echo "User name and password must be provided as arguments"
	exit 1
fi

mkdir -p config
docker run --rm \
  -e MAIL_USER=${MAIL_USER} \
  -e MAIL_PASS=${MAIL_PASS} \
  -ti tvial/docker-mailserver:latest \
  /bin/sh -c 'echo "$MAIL_USER|$(doveadm pw -s SHA512-CRYPT -u $MAIL_USER -p $MAIL_PASS)"' >> config/postfix-accounts.cf

