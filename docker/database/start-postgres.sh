#!/bin/bash

BASE_DIR=$(readlink -f $(dirname $0))

docker run \
	--name chess-postgres-01   \
	-e POSTGRES_USER=chess     \
	-e POSTGRES_PASSWORD=chess \
	-e POSTGRES_DB=chess1      \
	-p 5432:5432               \
	-v $BASE_DIR/data:/var/lib/postgresql/data \
	-d postgres:9.5

