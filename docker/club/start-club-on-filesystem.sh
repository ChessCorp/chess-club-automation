#!/bin/bash

docker run \
	--name chess-club-01    \
	-p 80:8080              \
	-v $PWD/data:/data      \
	-d chesscorp/chess-club
